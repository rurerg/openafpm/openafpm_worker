if rotor == 1  || rotor == 0

% draw fisrt magnet
mi_drawrectangle(dist_magnet/2,hr+g+tw+g,dist_magnet/2+wm,hr+g+tw+g+hm);
% set properties for all segments of the first magnet and place in group 3
mi_selectsegment(dist_magnet/2,hr+g+tw+g+1);
mi_setsegmentprop('',msize,0,0,3);
mi_clearselected
mi_selectsegment(dist_magnet/2+wm-1,hr+g+tw+g+hm);
mi_setsegmentprop('',msize,0,0,3);
mi_clearselected
mi_selectsegment(dist_magnet/2+wm-1,hr+g+tw+g);
mi_setsegmentprop('',msize,0,0,3);
mi_clearselected
mi_selectsegment(dist_magnet/2+wm,hr+g+tw+g+1);
mi_setsegmentprop('',msize,0,0,3);
mi_clearselected
% select the group
mi_selectgroup(3);
% copy another 4 times in order to create all magnets
mi_copytranslate(wm+dist_magnet,0,magnet_FEMM-1);
mi_clearselected

elseif rotor ==2
    
% draw fisrt magnet
mi_drawrectangle(dist_magnet/2,hr+g+tw+g,dist_magnet/2+wm,hr+g+tw+g+hm);
% set properties for all segments of the first magnet and place in group 3
mi_selectsegment(dist_magnet/2,hr+g+tw+g+1);
mi_setsegmentprop('',msize,0,0,3);
mi_clearselected
mi_selectsegment(dist_magnet/2+wm-1,hr+g+tw+g+hm);
mi_setsegmentprop('',msize,0,0,3);
mi_clearselected
mi_selectsegment(dist_magnet/2+wm-1,hr+g+tw+g);
mi_setsegmentprop('',msize,0,0,3);
mi_clearselected
mi_selectsegment(dist_magnet/2+wm,hr+g+tw+g+1);
mi_setsegmentprop('',msize,0,0,3);
mi_clearselected
% select the group
mi_selectgroup(3);
% copy another 4 times in order to create all magnets of top rotor
mi_copytranslate(wm+dist_magnet,0,magnet_FEMM-1);
mi_selectgroup(3);
% copy another 4 times in order to create all magnets of botom rotor
mi_mirror(0,hr+g+tw/2,L_half,hr+g+tw/2);
mi_clearselected

end