if rotor == 1 || rotor == 2 % move top magnets to the other side

mi_selectsegment(L_half+ypol_2-0.001,hr+g+tw+g+hm);% change group in extra part
mi_setsegmentprop('',msize,0,0,12);
mi_selectsegment(L_half+ypol_2-0.001,hr+g+tw+g);
mi_setsegmentprop('',msize,0,0,12);
mi_selectsegment(L_half+ypol_2,hr+g+tw+g+0.1);
mi_setsegmentprop('',msize,0,0,12);
mi_selectgroup(12);
mi_copytranslate(-L_half,0,1);% copy extra part to the other side
mi_clearselected
mi_selectsegment(ypol_2,hr+g+tw+g+hm-0.1);% change back to group 3 with rest of magnets
mi_setsegmentprop('',msize,0,0,3);
mi_clearselected

mi_selectsegment(L_half-0.1,hr+g+tw+g);
mi_setsegmentprop('',msize,0,0,11);
mi_selectsegment(L_half-0.1,hr+g+tw+g+hm);
mi_setsegmentprop('',msize,0,0,11);
mi_clearselected

mi_selectsegment(L_half+ypol_2-0.001,hr+g+tw+g+hm);
mi_selectsegment(L_half+ypol_2-0.001,hr+g+tw+g);
mi_selectsegment(L_half+ypol_2,hr+g+tw+g+0.1);
mi_deleteselected
mi_selectnode(L_half+ypol_2,hr+g+tw+g+hm);
mi_selectnode(L_half+ypol_2,hr+g+tw+g);
mi_deleteselected

mi_selectnode(L_half,hr+g+tw+g);
mi_movetranslate(0,hm);
mi_clearselected

mi_selectsegment(0.001,hr+g+tw+g);
mi_deleteselected

mi_selectnode(0,hr+g+tw+g);
mi_movetranslate(0,hm);
mi_clearselected

mi_selectsegment(ypol_2+0.1,hr+g+tw+g);
mi_setsegmentprop('',msize,0,0,3);
mi_clearselected

mi_selectlabel(0.001,hr+g+tw+g+hm/2);
mi_setblockprop(mag_mater,0,msize,'',mag_dir,3,0);
mi_movetranslate(wm/2+ypol_2,0);
mi_clearselected

mi_selectlabel(L_half-0.001,hr+g+tw+g+hm/2);
mi_deleteselected

end

if rotor == 2 % move bottom magnets to the other side
    
mi_selectsegment(L_half+ypol_2-0.001,hr); % change group in extra part
mi_setsegmentprop('',msize,0,0,19);
mi_selectsegment(L_half+ypol_2-0.001,hr-hm);
mi_setsegmentprop('',msize,0,0,19);
mi_selectsegment(L_half+ypol_2,hr-hm+0.1);
mi_setsegmentprop('',msize,0,0,19);
mi_selectgroup(19);
mi_copytranslate(-L_half,0,1); % copy extra part to the other side
mi_clearselected
mi_selectsegment(ypol_2,hr-0.1); % change back to group 3 with rest of magnets
mi_setsegmentprop('',msize,0,0,3);
mi_clearselected

mi_selectsegment(L_half-0.1,hr-hm);
mi_setsegmentprop('',msize,0,0,11);
mi_selectsegment(L_half-0.1,hr);
mi_setsegmentprop('',msize,0,0,11);
mi_clearselected

mi_selectsegment(L_half+ypol_2-0.001,hr);
mi_selectsegment(L_half+ypol_2-0.001,hr-hm);
mi_selectsegment(L_half+ypol_2,hr-hm+0.1);
mi_deleteselected
mi_selectnode(L_half+ypol_2,hr);
mi_selectnode(L_half+ypol_2,hr-hm);
mi_deleteselected

mi_selectnode(L_half,hr);
mi_movetranslate(0,-hm);
mi_clearselected

mi_selectsegment(0.001,hr);
mi_deleteselected

mi_selectnode(0,hr);
mi_movetranslate(0,-hm);
mi_clearselected

mi_selectsegment(ypol_2+0.1,hr-hm);
mi_setsegmentprop('',msize,0,0,3);
mi_clearselected

mi_selectlabel(0.001,hr-hm/2);
mi_setblockprop(mag_mater,0,msize,'',mag_dir,3,0);
mi_movetranslate(wm/2+ypol_2,0);
mi_clearselected

mi_selectlabel(L_half-0.001,hr-hm/2);
mi_deleteselected

end

if rotor == 0 % move top magnets to the other side
    
mi_selectsegment(L_half+ypol_2-0.001,extra+hr+g+tw+g+hm);% change group in extra part
mi_setsegmentprop('',msize,0,0,12);
mi_selectsegment(L_half+ypol_2-0.001,extra+hr+g+tw+g);
mi_setsegmentprop('',msize,0,0,12);
mi_selectsegment(L_half+ypol_2,extra+hr+g+tw+g+0.1);
mi_setsegmentprop('',msize,0,0,12);
mi_selectgroup(12);
mi_copytranslate(-L_half,0,1);% copy extra part to the other side
mi_clearselected
mi_selectsegment(ypol_2,extra+hr+g+tw+g+hm-0.1);% change back to group 3 with rest of magnets
mi_setsegmentprop('',msize,0,0,3);
mi_clearselected

mi_selectsegment(L_half-0.1,extra+hr+g+tw+g);
mi_setsegmentprop('',msize,0,0,11);
mi_selectsegment(L_half-0.1,extra+hr+g+tw+g+hm);
mi_setsegmentprop('',msize,0,0,11);
mi_clearselected

mi_selectsegment(L_half+ypol_2-0.001,extra+hr+g+tw+g+hm);
mi_selectsegment(L_half+ypol_2-0.001,extra+hr+g+tw+g);
mi_selectsegment(L_half+ypol_2,extra+hr+g+tw+g+0.1);
mi_deleteselected
mi_selectnode(L_half+ypol_2,extra+hr+g+tw+g+hm);
mi_selectnode(L_half+ypol_2,extra+hr+g+tw+g);
mi_deleteselected

mi_selectnode(L_half,extra+hr+g+tw+g);
mi_movetranslate(0,hm);
mi_clearselected

mi_selectsegment(0.001,extra+hr+g+tw+g);
mi_deleteselected

mi_selectnode(0,extra+hr+g+tw+g);
mi_movetranslate(0,hm);
mi_clearselected

mi_selectsegment(ypol_2+0.1,extra+hr+g+tw+g);
mi_setsegmentprop('',msize,0,0,3);
mi_clearselected

mi_selectlabel(0.001,extra+hr+g+tw+g+hm/2);
mi_setblockprop(mag_mater,0,msize,'',mag_dir,3,0);
mi_movetranslate(wm/2+ypol_2,0);
mi_clearselected

mi_selectlabel(L_half-0.001,extra+hr+g+tw+g+hm/2);
mi_deleteselected

end
