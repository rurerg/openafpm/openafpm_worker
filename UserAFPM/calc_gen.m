%%%%%%%%%%%%% environment setup %%%%%%%%%%%%%

addpath('~/.wine/drive_c/femm42/mfiles'); % add FEMM mfiles to MATLAB

input_vars
%%%%%%%%%% Input parameters  %%%%%%%%%%%%%
run_solver=true; % UserAFPM always uses solver

% Unused variables to be removed
unused_vars = ['addition'
'angle'
'ans'
'Bavg'
'BBB'
'Joule'
'm3'
'Bmavg_real'
'BMAX'
'Bmax_real'
'BMG'
'Bmg_real'
'Bp_real'
'Bp_real'
'Bpmax'
'break_counter'
'Brms'
'coil_FEMM'
'coil_percent'
'coil_side_dist'
'coil_spacing'
'coilangle_Rin'
'coilangle_Rout'
'counter'
'counter_energy'
'd'
'delta'
'deltaN'
'depth'
'displacement'
'Efficiency'
'extra'
'f'
'Flag'
'Flux_pole_BOOK'
'Fluxoc'
'Fs'
'g_init'
'i'
'Imax'
'j'
'k'
'L_half'
'lavgHP'
'lec'
'mag_dir'
'magnet_FEMM'
'msize'
'Nc_init'
'NFFT'
'Overall_Energy_per_Volume'
'paragogost'
'path'
'Phl'
'Plosses'
'Q'
'Radius'
'ResHP_20'
'ResHP_70'
'Rin_coil_spacing'
'Rin_init'
'rts'
'segment'
'SIZE'
'Speed'
'sumA'
'sumB'
'sumC'
'sumsquareA'
'sumsquareB'
'sumsquareC'
'temp_var'
'tempvar'
'vector_Force'
'vectorA'
'vectorB'
'vectorC'
'wc_theor'
'x'
'xx'
'y'
'Y'
'ypol'
'ypol_2'
'ypol_3'
'yy',
'mag_mater',
'Optimal_airgap',
'g_max_energy',
'Max_EperV',
'Block_Energy_per_Volume',
'location_path',
'unused_vars'];

% run sim
calc_gen_WindEng_15_12_17_UserAFPM;

% cleanup variables
display 'Removing unused variables'
for i = 1 : rows(unused_vars)
  if eval(sprintf('exist %s\n', unused_vars(i,:))) == 1
    eval(sprintf('clear %s\n', unused_vars(i,:)));
  endif
endfor
%save variables
display 'Storing variables'
eval(sprintf('save -mat7-binary %s/variables.mat',sim_output_path));

%generate JSON data
status = 'success';
status_message = '';

display 'Generating JSON output'
JSON_Output;

filename = [sim_output_path,'/result.json'];
fid = fopen (filename, 'w');
fputs (fid, json);
fclose (fid);

display 'Simulation complete'
