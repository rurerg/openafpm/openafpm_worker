tempvar=size(fluxlinkageB); % derivative with method of differences for phase B 
SIZE=tempvar(1,2);

i=1;
LOGOSB(j,1)=q*poles/2*2*pi*speed/60*max(abs(fluxlinkageB(j,:)))*fluxlinkageB(j,1)/time(j,1);

for i=2:SIZE
    paragogost(i)=(fluxlinkageB(j,i)-fluxlinkageB(j,i-1))/(time(j,i)-time(j,i-1));
    LOGOSB(j,i)=q*paragogost(i);
end

sumB=0;
for i=2:SIZE
    sumB=sumB+LOGOSB(j,i)^2;
end
V_rmsB=sqrt(sumB/SIZE); % ouput rms value of EMF in phase B

tempvar=size(fluxlinkageA); % derivative with method of differences for phase A 
SIZE=tempvar(1,2);

i=1;
LOGOSA(j,1)=q*poles/2*2*pi*speed/60*max(abs(fluxlinkageA(j,:)))*fluxlinkageA(j,1)/time(j,1);

for i=2:SIZE
    paragogost(i)=(fluxlinkageA(j,i)-fluxlinkageA(j,i-1))/(time(j,i)-time(j,i-1));
    LOGOSA(j,i)=q*paragogost(i);
end

sumA=0;
for i=2:SIZE
    sumA=sumA+LOGOSA(j,i)^2;
end
V_rmsA=sqrt(sumA/SIZE); % ouput rms value of EMF in phase A

tempvar=size(fluxlinkageC); % derivative with method of differences for phase C 
SIZE=tempvar(1,2);

i=1;
LOGOSC(j,1)=q*poles/2*2*pi*speed/60*max(abs(fluxlinkageC(j,:)))*fluxlinkageC(j,1)/time(j,1);

for i=2:SIZE
    paragogost(i)=(fluxlinkageC(j,i)-fluxlinkageC(j,i-1))/(time(j,i)-time(j,i-1));
    LOGOSC(j,i)=q*paragogost(i);
end

sumC=0;
for i=2:SIZE
    sumC=sumC+LOGOSC(j,i)^2;
end
V_rmsC=sqrt(sumC/SIZE); % ouput rms value of EMF in phase C
