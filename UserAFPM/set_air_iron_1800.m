if rotor == 1

% set block properties for rotor disks and air

mi_addblocklabel(L_half/2,hr/2);
mi_selectlabel(L_half/2,hr/2);
mi_setblockprop('Pure Iron',automesh,msize,'',0,9,0);
mi_clearselected


mi_addblocklabel(L_half/2,hr+g+tw+g+hm+hr/2);
mi_selectlabel(L_half/2,hr+g+tw+g+hm+hr/2);
mi_setblockprop('Pure Iron',automesh,msize,'',0,9,0);
mi_clearselected


mi_addblocklabel(L_half/2,hr+g+tw/2);
mi_selectlabel(L_half/2,hr+g+tw/2);
mi_setblockprop('Air',automesh,msize,'',0,23,0);
mi_clearselected

elseif rotor == 2 

% set block properties for rotor disks and air

mi_addblocklabel(L_half/2,(hr-hm)/2);
mi_selectlabel(L_half/2,(hr-hm)/2);
mi_setblockprop('Pure Iron',automesh,msize,'',0,9,0);
mi_clearselected


mi_addblocklabel(L_half/2,(hr-hm)+hm+g+tw+g+hm+(hr-hm)/2);
mi_selectlabel(L_half/2,(hr-hm)+hm+g+tw+g+hm+(hr-hm)/2);
mi_setblockprop('Pure Iron',automesh,msize,'',0,9,0);
mi_clearselected


mi_addblocklabel(L_half/2,hr+g+tw/2);
mi_selectlabel(L_half/2,hr+g+tw/2);
mi_setblockprop('Air',automesh,msize,'',0,23,0);
mi_clearselected

elseif rotor == 0
    
% set block properties for rotor disks and air

mi_addblocklabel(L_half/2,(hr-hm)+hm+g+tw+g+hm+(hr-hm)/2);
mi_selectlabel(L_half/2,(hr-hm)+hm+g+tw+g+hm+(hr-hm)/2);
mi_setblockprop('Pure Iron',automesh,msize,'',0,9,0);
mi_clearselected


mi_addblocklabel(L_half/2,hr+g+tw/2);
mi_selectlabel(L_half/2,hr+g+tw/2);
mi_setblockprop('Air',automesh,msize,'',0,23,0);
mi_clearselected

end

