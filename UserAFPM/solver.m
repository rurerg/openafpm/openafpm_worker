depth=la; % 'la' effective length is the depth of FEMM 2D - in this case 30mm
msize=1; % Local element size no greater than msize in FEMM
automesh=1; % (0) Consider mesh size (1) automesh

if Imax==0 % loop only at opencircuit
    Irms=0; % zero rms current
    temp_var=1; % set to 1 just in order to make one loop in j
else
    Irms=0:I_step:Imax; % else currents will go from 0 to Imax in steps of I_step, create matrix
    temp_var=(Imax/I_step+1); % set number of current loops to make in j
end

j=1; % this variable is used to count steps in current changes

if rotor == 2  || rotor == 0 % set hr=hr+hm for easy construction of second rotor
    hr=hr+hm;
end

%for j=1:(Imax+1) % loop for all current values
for j=1:temp_var % loop for all current values according to step

d=0; % this variable is used as a count of the steps

openfemm; % open FEMM
newdocument(0); % new document
mi_probdef(0,'millimeters','planar',1E-8,depth,30,0); % define problem
msize=1; % Local element size no greater than msize

drawcoils_1800 % draws coils in FEMM

if rotor == 0 % extra space in the case of rotor (0) in order to allow for magnetic field to expand
    mem=tw;
    tw=tw+extra;
end

drawrotordisks_1800 % draws rotor disks in FEMM
drawmagnets_1800 % draws magnets in FEMM

addmaterials_1800 % adds materials to be used in the problem
addcircuits_1800 % adds three phases of the circuit
addbounds_1800 % adds boundary conditions of arrangement

setmagnets_1800 % set magnets properties

if rotor == 0 % set tw back to original value
    tw=mem;
end

setcoils_1800 % set coil properties

if rotor == 0 % extra space in the case of rotor (0) in order to allow for magnetic field to expand
    tw=tw+extra;
end

drawbounds_1800 % draw and set boundary conditions
set_air_iron_1800 % set properties for air and rotor disks

if rotor == 0 % set tw back to original value
    tw=mem;
end

mi_saveas([location_path, 'temp.fem']); % temporary FEMM file

sumsquareA=0; % variables for calulating RMS voltages
sumsquareB=0;
sumsquareC=0;
counter=1; % counts number of time flux linkage is calculated for a certain value of current

displacement=0; % initialize displacement of rotor
d=d+1; % counter of steps
flux_linkage % calculation of flux linkage for initial position
BpCALC % calculate Bp for inital position

% move until first magnet reaches boundary according to step
i=1;
for i=1:floor((dist_magnet/2)/step)

selectrotor_1800 % select magents
mi_movetranslate(step,0); % move them by one step
displacement=displacement+step; % add this step to displacment
i=i+1; % counter for loop
d=d+1; % counter of steps
counter=counter+1; % increase for next flux linkage measurement point in time
flux_linkage % calculations of flux linkage

end

% move magnets one more time reaching out of boundary
selectrotor_1800
mi_movetranslate(step,0);
displacement=displacement+step;
% length of magnet reaching out of boundary according to step
ypol=round_2dec(step-((dist_magnet/2)-floor((dist_magnet/2)/step)*step));
% move magnet reaching out of boundary to the other side
mag_dir=-90; % set direction of magetic field for this magnet
movemagnet_ypol_1800 % move part of the magnet to opposite side
% take masurements for this step
d=d+1;
counter=counter+1; % increase for next flux linkage measurement point in time
flux_linkage

k=1; % counter for loop

for k=1:magnet_FEMM/2 % move magnets for one electric period

% move until other side of magnet reaches boundary according to step
i=1;
for i=1:floor((wm-ypol)/step)

selectrotor_1800
mi_movetranslate(step,0);
displacement=displacement+step;
d=d+1;
counter=counter+1; % increase for next flux linkage measurement point in time
flux_linkage
i=i+1;

end

% move magnets one more time reaching out of boundary
selectrotor_1800
mi_movetranslate(step,0);
displacement=displacement+step;
% length of magnet reaching out of boundary according to step
ypol_2=round_2dec(step-(wm-ypol-floor((wm-ypol)/step)*step));
movemagnet_ypol_2_1800
% take masurements for this step
d=d+1;
counter=counter+1; % increase for next flux linkage measurement point in time
flux_linkage

if k==magnet_FEMM/2
    break
end

% move until second magnet reaches boundary according to step
i=1;
for i=1:floor((dist_magnet-ypol_2)/step)

selectrotor_1800
mi_movetranslate(step,0);
displacement=displacement+step;
d=d+1;
counter=counter+1; % increase for next flux linkage measurement point in time
flux_linkage
i=i+1;

end

if mag_dir==-90 % reverse magnetic field direction for the next magnet
        mag_dir=90;
    else if mag_dir==90;
            mag_dir=-90;
        end
end

% move magnets one more time reaching out of boundary
selectrotor_1800
mi_movetranslate(step,0);
displacement=displacement+step;
% length of magnet reaching out of boundary according to step
ypol=round_2dec(step-(dist_magnet-ypol_2-floor((dist_magnet-ypol_2)/step)*step));
% move magnet reaching out of boundary to the other side
movemagnet_ypol_1800
% take masurements for this step
d=d+1;
counter=counter+1; % increase for next flux linkage measurement point in time
flux_linkage


k=k+1;
end

% move until first magnet reaches initial position according to step
i=1;
for i=1:floor(((dist_magnet/2)-ypol_2)/step)

selectrotor_1800
mi_movetranslate(step,0);
displacement=displacement+step;
d=d+1;
counter=counter+1; % increase for next flux linkage measurement point in time
flux_linkage
i=i+1;

end

% final step with what is left to complete 360 degrees
ypol_3=round_2dec(L_half/2-floor((L_half/2)/step)*step);  
selectrotor_1800
mi_movetranslate(ypol_3,0);
displacement=displacement+ypol_3;
d=d+1;
counter=counter+1; % increase for next flux linkage measurement point in time
flux_linkage

% calculate RMS for Flux linkage
FluxRmsA=sqrt(sumsquareA/counter);
FluxRmsB=sqrt(sumsquareB/counter);
FluxRmsC=sqrt(sumsquareC/counter);
FluxRms(j)=(FluxRmsA+FluxRmsB+FluxRmsC)/3;

if step>3 % use derivative only for small steps
    % calculate RMS Voltages
    Varms=q*FluxRmsA*poles/2*2*pi*speed/60;
    Vbrms=q*FluxRmsB*poles/2*2*pi*speed/60;
    Vcrms=q*FluxRmsC*poles/2*2*pi*speed/60;
    V_rms(j)=(Varms+Vbrms+Vcrms)/3;
    %V_rms=Vrms(j);
else
    deriv_rms % calculate the rms value of Voltage and EMF voltage waveform from derivative of Flux linkage
    V_rms(j)=(V_rmsA+V_rmsB+V_rmsC)/3;
end

if j==1 % for the first itteration with no load current calculate rms for EMF
    Ef=V_rms(j);
end


end

if rotor == 2  || rotor == 0 % set hr back to its original value
    hr=hr-hm;
end
