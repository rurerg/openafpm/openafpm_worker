#!/usr/bin/env python
# import web
import config
import sentry_sdk
sentry_sdk.init(config.sentry_dsn)

import os
import json
import requests
import poller
import pinger
import config
import subprocess
import time
import threading
import time

##
# Global settings
#
try:
  os.mkdir(config.workspace_path)
except Exception as e:
  print "Global Workspace exists"
else:
  print "Created workspace "+config.workspace_path

print "Starting Xvfb"
xvfb = subprocess.Popen(['Xvfb', ':3', '-screen','0', '1024x768x16'])

time.sleep(1)

print "Starting VNC server"
subprocess.Popen(['x11vnc', '-display', ':3', '-nopw', '-shared', '-viewonly', '-q', '-forever'])
##
# Start poller
#
poller.start()
pinger.start()

##
#Loop while threads run
#
while threading.active_count() > 0:
    time.sleep(1)
