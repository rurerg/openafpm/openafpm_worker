if rotor == 1 || rotor == 2

% add block property in the middle of block
mi_addblocklabel(coil_spacing/2+wc/2,hr+g+tw/2);

% set direction of winding (currents) in the coils of the phases - (+) into
% screen and (-) out of screen
mi_selectlabel(coil_spacing/2+wc/2,hr+g+tw/2);
mi_setblockprop('dc mm',automesh,msize,'A',0,2,Nc);
mi_copytranslate(coil_whole+wc,0,1);
mi_selectlabel(coil_spacing/2+coil_whole+wc+wc/2,hr+g+tw/2);
mi_setblockprop('dc mm',automesh,msize,'A',0,2,-Nc);
mi_copytranslate(coil_spacing+wc,0,1);

mi_selectlabel(coil_spacing/2+coil_whole+wc+wc/2+coil_spacing+wc,hr+g+tw/2);
mi_setblockprop('dc mm',automesh,msize,'B',0,2,Nc);
mi_copytranslate(coil_whole+wc,0,1);
mi_selectlabel(coil_spacing/2+(coil_whole+wc)*2+wc/2+coil_spacing+wc,hr+g+tw/2);
mi_setblockprop('dc mm',automesh,msize,'B',0,2,-Nc);
mi_copytranslate(coil_spacing+wc,0,1);

mi_selectlabel(coil_spacing/2+(coil_whole+wc)*2+wc/2+(coil_spacing+wc)*2,hr+g+tw/2);
mi_setblockprop('dc mm',automesh,msize,'C',0,2,Nc);
mi_copytranslate(coil_whole+wc,0,1);
mi_selectlabel(coil_spacing/2+(coil_whole+wc)*3+wc/2+(coil_spacing+wc)*2,hr+g+tw/2);
mi_setblockprop('dc mm',automesh,msize,'C',0,2,-Nc);

mi_clearselected

elseif rotor == 0
    
% add block property in the middle of block
mi_addblocklabel(coil_spacing/2+wc/2,extra+hr+g+tw/2);

% set direction of winding (currents) in the coils of the phases - (+) into
% screen and (-) out of screen
mi_selectlabel(coil_spacing/2+wc/2,extra+hr+g+tw/2);
mi_setblockprop('dc mm',automesh,msize,'A',0,2,Nc);
mi_copytranslate(coil_whole+wc,0,1);
mi_selectlabel(coil_spacing/2+coil_whole+wc+wc/2,extra+hr+g+tw/2);
mi_setblockprop('dc mm',automesh,msize,'A',0,2,-Nc);
mi_copytranslate(coil_spacing+wc,0,1);

mi_selectlabel(coil_spacing/2+coil_whole+wc+wc/2+coil_spacing+wc,extra+hr+g+tw/2);
mi_setblockprop('dc mm',automesh,msize,'B',0,2,Nc);
mi_copytranslate(coil_whole+wc,0,1);
mi_selectlabel(coil_spacing/2+(coil_whole+wc)*2+wc/2+coil_spacing+wc,extra+hr+g+tw/2);
mi_setblockprop('dc mm',automesh,msize,'B',0,2,-Nc);
mi_copytranslate(coil_spacing+wc,0,1);

mi_selectlabel(coil_spacing/2+(coil_whole+wc)*2+wc/2+(coil_spacing+wc)*2,extra+hr+g+tw/2);
mi_setblockprop('dc mm',automesh,msize,'C',0,2,Nc);
mi_copytranslate(coil_whole+wc,0,1);
mi_selectlabel(coil_spacing/2+(coil_whole+wc)*3+wc/2+(coil_spacing+wc)*2,extra+hr+g+tw/2);
mi_setblockprop('dc mm',automesh,msize,'C',0,2,-Nc);

mi_clearselected

end