% calculates the magnitude of the first harmonic of the normal flux density

if rotor == 1 || rotor == 2
% draw line at the top of the coil
%mo_addcontour(0,hr+g+tw); % draw contour for one complete period of Bn normal flux density
%mo_addcontour(dist_magnet*2+wm*2,hr+g+tw);
% draw line at 45% of the coil from the bottom
mo_addcontour(0,hr+g+tw*0.5); % draw contour for half a period of Bn normal flux density
mo_addcontour(dist_magnet+wm,hr+g+tw/2);
% draw line at the middle of the coil
%mo_addcontour(0,hr+g+tw/2); % draw contour for half a period of Bn normal flux density
%mo_addcontour(dist_magnet+wm,hr+g+tw/2);
% draw line at the middle of the mechanical clearence gap
%mo_addcontour(0,hr+g+tw+g/2); % draw contour for half a period of Bn normal flux density
%mo_addcontour(dist_magnet+wm,hr+g+tw/2);
% draw line at the middle of the coil
%mo_addcontour(0,hr+g+tw/2); % draw contour for two complete periods of Bn normal flux density
%mo_addcontour(L_half,hr+g+tw/2);

elseif rotor == 0
% draw line at the middle of the coil (not sure if this is correct)
mo_addcontour(0,extra+hr+g+tw/2); % draw contour for one complete period of Bn normal flux density
mo_addcontour(dist_magnet*2+wm*2,extra+hr+g+tw/2);
%mo_addcontour(L_half,extra+hr+g+tw/2); % draw contour for two periods

end

path=[location_path,'Bmaxfile.txt'];
mo_makeplot(2,150,path,1); % make plot of Bn with 150 points and save file
%mo_makeplot(2,150); % make plot of Bn with 150 points and save file

Bmaxfile = load(path); % load data
x = Bmaxfile(:,1); % load firts column in x
BMAX = Bmaxfile(:,2); % load second column in BBB

%calculate Bavg and Bmg(Bmax) in airgap as in AFPM book page38
SIZE=size(BMAX); % find average value
addition=0;
for i=1:(SIZE(1,1))
    addition=BMAX(i,1)+addition;
end
Bavg=addition/SIZE(1,1);
Bmax=max(BMAX); %find maximum value

mo_clearcontour(); % clear contour for a complete period of Bn normal flux density
mo_clearcontour();
