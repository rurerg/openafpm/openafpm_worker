%%%%%%%%%%%%% environment setup %%%%%%%%%%%%%

clear all % clear all previous variables in MATLAB
addpath('c:/femm42/mfiles'); % add FEMM mfiles to MATLAB
%addpath('~/.wine/drive_c/femm42/mfiles'); % add FEMM mfiles to MATLAB
location_path=['c:/AFPM_mfiles/OpenAFPM/OpenAFPM_3_1_18/MagnAFPM/']; % path location to write files and read from
%location_path=['/tmp/']; % path location to write files and read from

%%%%%%%%%% Input parameters  %%%%%%%%%%%%%
run_solver=false;
automesh=1; % (0) Use fine mesh (1) Automesh when running solver but not Bp_ai_kd_WindEng and CALC_hr_saturation this always fine

current_step=1; % step to increase current by - User defined with values 1,2,3,4 and 5
RPM_step=1; % step to increase rpm by - User defined with values 1,5,10 and 20

% displacement step in mm, choose from 1,2,3 etc - check angle in deg to be 1 degree or less
step=2;

% choose between single or double rotor topology
rotor=2; % double rotor (2) or single rotor with metal disk (1) or single rotor (0) 

% Choose stator winding type
coil_type=1; % choose (1) for rectangular coils or (2) for keyhole coils or (3) for triangular coils 

if coil_type==3
    winder_bolt_size=8; % coil winder bolt size at Rin for trianular coils (type 3)
end

% Calculate stator thickness tw or use user defined
% only for rotor=2
stator_thick=1; % (1) user defined (2) calculate 

% Calculate rotor disk thickness hr or use user defined
rotor_thick=1; % (1) user defined (2) calculate 
% This result will tell you which is the min hr for no saturation but you
% will need to add a few mm in order to avoid deflection, especially for
% large Rout

% Calculate fill factor kf or use user defined
fill_factor=1; % (1) user defined (2) calculate 

% magnetic material http://www.ndfeb-info.com/neodymium_grades.aspx
mag_mater='NdFeB N40'; % choose magnetic material between 'NdFeB N40/N42/N45' and 'Ferrite C8' 

% mechanical clearence + effective length + magnet size
g=3; % mechanical clearence gap including resin layers 0.5mm on coil and 0.5mm on magnet
la=46; % 'la' effective length and also magnet length
wm=30; % magnet width 

% blade rotor
Rturb=1.2; % turbine radius in meters
tsr_cut_in=8.75; % cut in tip speed ratio
Vw_cut_in=3; % cut in wind speed
Vw_nom=10; % rated-nominal windspeed
cp=0.3; % aerodynamic power coefficient 
design_tsr=7; % design tsr - optimal tip speed ratio for blades (between 6 and 7) use it for MPPT

% battery or wind inverter cutin voltage
V_batt=24; % battery voltage or lower part of VDC range of wind inverter
% battery or wind inverter nominal voltage
V_batt_nom=24; % battery voltage at nominal wind speed (this can be system voltage or charge controller dump voltage)

% rated power
air_dens=1.204; % air density at 20 degrees
trans_loss=10; % Percent losses on cables at 10m/s
rect_loss=10; % Percent losses on rectifier

poles=12; % number of poles
hm=10; % magnet thickness 
tw=13; % 'tw' stator thickness
Jmax=6; % rated current density
kf=0.55; % winding fill factor 
hr=10; % 'hr' rotor disk thickness

% Copper
pcu=8.94; % copper density g/cm3
copperprice=12; % in Euro/kg
p20=1.678*10^(-8); % specific resistance of copper at 20 deg

% Iron
ironprice=1; % in Euro/kg
pFe=7.87; % iron density in g/cm3

% Resin
resinprice=11.23; % in Euro/kg
presin=1.36; % in g/cm3

% Wood (for moulds)
pplywood=17.6; % Euro/m2 for 12mm

% run sim
calc_gen_WindEng_15_12_17_MagnAFPM

display 'Simulation complete'
