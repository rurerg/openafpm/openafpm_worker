% calculates the magnitude of the first harmonic of the normal flux density

if rotor == 1 || rotor == 2
% draw line at the middle of the coil
mo_addcontour(0,hr+g+tw/2); % draw contour for one complete period of Bn normal flux density
mo_addcontour(L_half,hr+g+tw/2);
% draw line at the top of the coil
%mo_addcontour(0,hr+g+tw+g/2); % draw contour for one complete period of Bn normal flux density
%mo_addcontour(dist_magnet*2+wm*2,hr+g+tw+g/2);
% draw line at the middle of the coil
%mo_addcontour(0,hr+g+tw/2); % draw contour for half a period of Bn normal flux density
%mo_addcontour(dist_magnet+wm,hr+g+tw/2);
% draw line at the middle of the coil
%mo_addcontour(0,hr+g+tw/2); % draw contour for two complete periods of Bn normal flux density
%mo_addcontour(L_half,hr+g+tw/2);

elseif rotor == 0
% draw line at the middle of the coil (not sure if this is correct)
mo_addcontour(0,extra+hr+g+tw/2); % draw contour for one complete period of Bn normal flux density
mo_addcontour(L_half,extra+hr+g+tw/2);
%mo_addcontour(L_half,extra+hr+g+tw/2); % draw contour for two periods

end

path=[location_path,'Bpfile.txt'];
mo_makeplot(2,150,path,1); % make plot of Bn with 150 points and save file
%mo_makeplot(2,150); % make plot of Bn with 150 points and save file


Bpfile = load(path); % load data
x = Bpfile(:,1); % load firts column in x
BBB = Bpfile(:,2); % load second column in BBB

% perform Fast Fourier Transform
NFFT = 2^nextpow2(150); % Next power of 2 from length of y
Y = abs(fft(BBB,NFFT))*2/150;
Fs=1000;
f = Fs/2*linspace(0,1,NFFT/2+1);
%plot(f,Y(1:NFFT/2+1)); % Plot single-sided amplitude spectrum

Bpmax(j)=Y(1); % find largest magnitude harmonic Bpmax
for i=1:150
   if Y(i)>=Bpmax(j)
        Bpmax(j)=Y(i);
    end
end

mo_clearcontour(); % clear contour for a complete period of Bn normal flux density
mo_clearcontour();
