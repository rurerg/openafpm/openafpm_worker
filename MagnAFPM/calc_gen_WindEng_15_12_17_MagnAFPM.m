% Radius to run simlation at
Radius_sim=1; % 1 for Ravg, 2 for Rin and 3 for Rout_eff

extra=40; % extra space in the case of rotor (0) in order to allow for magnetic field to expand
no_FEMM=0; % When to enter FEMM or not for Saturation calc and Screenshot

efficiency_real=0.8; % estimated generator efficiency at rated power
efficiency=0; % initialize variable

if strcmp(mag_mater,'Ceramic 8')
     Br=0.39; % T
     Hc=227950; % A/m from FEMM library
     BHmax=30; % KJ/m3
     mag_density=5; % density of Ferrite in g/cm3
end

if strcmp(mag_mater,'NdFeB N35')
     Br=1.195; % T
     Hc=155319*sqrt(35); % A/m from FEMM library
     BHmax=287; % KJ/m3
     mag_density=7.5; % density of NdFeB in g/cm3
end

if strcmp(mag_mater,'NdFeB N40')
     Br=1.265; % T
     Hc=155319*sqrt(40); % A/m from FEMM library
     BHmax=326; % KJ/m3
     mag_density=7.5; % density of NdFeB in g/cm3
end

if strcmp(mag_mater,'NdFeB N42')
     Br=1.300; % T
     Hc=155319*sqrt(42); % A/m from FEMM library
     BHmax=342; % KJ/m3
     mag_density=7.5; % density of NdFeB in g/cm3
end

if strcmp(mag_mater,'NdFeB N45')
     Br=1.350; % T
     Hc=155319*sqrt(45); % A/m from FEMM library
     BHmax=366; % KJ/m3
     mag_density=7.5; % density of NdFeB in g/cm3
end

if strcmp(mag_mater,'NdFeB N52')
     Br=1.450; % T
     Hc=155319*sqrt(52); % A/m from FEMM library
     BHmax=422; % KJ/m3
     mag_density=7.5; % density of NdFeB in g/cm3
end

% Flux density on magnet surface from HP recipe and FEMM calc
if rotor==2 && strcmp(mag_mater,'NdFeB N40') % for double rotor
    Bmg_real=0.62; % Initialize parameter for NdFeB
    Bp_real=0.4; % Intialize parameter as a value smaller than Bmg_real
elseif rotor==2 && strcmp(mag_mater,'NdFeB N35') % for double rotor
    Bmg_real=0.62; % Initialize parameter for NdFeB
    Bp_real=0.4; % Intialize parameter as a value smaller than Bmg_real
elseif rotor==2 && strcmp(mag_mater,'NdFeB N42') % for double rotor
    Bmg_real=0.62; % Initialize parameter for NdFeB
    Bp_real=0.4; % Intialize parameter as a value smaller than Bmg_real
elseif rotor==2 && strcmp(mag_mater,'NdFeB N45') % for double rotor
    Bmg_real=0.62; % Initialize parameter for NdFeB
    Bp_real=0.4; % Intialize parameter as a value smaller than Bmg_real
elseif rotor==2 && strcmp(mag_mater,'NdFeB N52') % for double rotor
    Bmg_real=0.62; % Initialize parameter for NdFeB
    Bp_real=0.4; % Intialize parameter as a value smaller than Bmg_real
elseif rotor==2 && strcmp(mag_mater,'Ceramic 8') % for double rotor
    Bmg_real=0.237; % Initialize parameter for Ferrite C8
    Bp_real=0.1; % Intialize parameter as a value smaller than Bmg_real
end

if rotor==1 && strcmp(mag_mater,'NdFeB N40') % for single rotor and metal disk
    Bmg_real=0.44; % Initialize parameter for NdFeB
    Bp_real=0.2; % Intialize parameter as a value smaller than Bmg_real
elseif rotor==1 && strcmp(mag_mater,'NdFeB N35') % for single rotor and metal disk
    Bmg_real=0.44; % Initialize parameter for NdFeB
    Bp_real=0.2; % Intialize parameter as a value smaller than Bmg_real
elseif rotor==1 && strcmp(mag_mater,'NdFeB N42') % for single rotor and metal disk
    Bmg_real=0.44; % Initialize parameter for NdFeB
    Bp_real=0.2; % Intialize parameter as a value smaller than Bmg_real
elseif rotor==1 && strcmp(mag_mater,'NdFeB N45') % for single rotor and metal disk
    Bmg_real=0.44; % Initialize parameter for NdFeB
    Bp_real=0.2; % Intialize parameter as a value smaller than Bmg_real
elseif rotor==1 && strcmp(mag_mater,'NdFeB N52') % for single rotor and metal disk
    Bmg_real=0.44; % Initialize parameter for NdFeB
    Bp_real=0.2; % Intialize parameter as a value smaller than Bmg_real
elseif rotor==1 && strcmp(mag_mater,'Ceramic 8') % for single rotor and metal disk
    Bmg_real=0.176; % Initialize parameter for Ferrite C8
    Bp_real=0.1; % Intialize parameter as a value smaller than Bmg_real
end

if rotor==0 && strcmp(mag_mater,'NdFeB N40') % for single rotor
    Bmg_real=0.3; % Initialize parameter for NdFeB
    Bp_real=0.1; % Intialize parameter as a value smaller than Bmg_real
elseif rotor==0 && strcmp(mag_mater,'NdFeB N35') % for single rotor
    Bmg_real=0.3; % Initialize parameter for NdFeB
    Bp_real=0.1; % Intialize parameter as a value smaller than Bmg_real
elseif rotor==0 && strcmp(mag_mater,'NdFeB N42') % for single rotor
    Bmg_real=0.3; % Initialize parameter for NdFeB
    Bp_real=0.1; % Intialize parameter as a value smaller than Bmg_real
elseif rotor==0 && strcmp(mag_mater,'NdFeB N45') % for single rotor
    Bmg_real=0.3; % Initialize parameter for NdFeB
    Bp_real=0.1; % Intialize parameter as a value smaller than Bmg_real
elseif rotor==0 && strcmp(mag_mater,'NdFeB N52') % for single rotor
    Bmg_real=0.3; % Initialize parameter for NdFeB
    Bp_real=0.1; % Intialize parameter as a value smaller than Bmg_real
elseif rotor==0 && strcmp(mag_mater,'Ceramic 8') % for single rotor
    Bmg_real=0.14; % Initialize parameter for Ferrite C8
    Bp_real=0.1; % Intialize parameter as a value smaller than Bmg_real
end

% initialize variables
Bmax_real=Bmg_real-0.08;
Bp=0;
Bmg=0;
Bmax=0;
break_counter=0; % counter to break after 15 iterations in while loop
tw_Flag=1;
kf_Flag=1;

cq=0.3; % initial value for the heat coefficient

Flux_pole_BOOK=Bmg_real*wm*(10^-3)*la*(10^-3); % initialize

kw=0.95; % initialize 
kw_counter=0;

while (kw_counter<=2 || abs(efficiency_real-efficiency)>0.01) || (kw_counter<=2 || abs(Bp_real-Bp)>0.001) || (kw_counter<=2 || abs(Bmg_real-Bmg)>0.001) %|| (abs(Bmg_real-(Br/2.1))>0.001)

efficiency=efficiency_real; % part of the efficiency loop
Bp=Bp_real; % part of the Bp loop
Bmg=Bmg_real; %  part of the Bmg loop
Bmax=Bmax_real;

% function round_2dec rounds to the fourth decimal point

% blade rotor
n_cut_in=(60*Vw_cut_in*tsr_cut_in)/(2*3.14*Rturb); % cut in rpm

% rated power
Pnom=0.5*air_dens*cp*efficiency*3.14*(Rturb^2)*Vw_nom^3; % rated power in watts
Pnom_rect=Pnom*((100-trans_loss)/100);
Pnom_dc=Pnom_rect*((100-rect_loss)/100);

% EMf at cut-in
EMF_cut_in=V_batt/(sqrt(3)*1.35); % EMF at cut-in (not including voltage drop 1.4V at rectifier)

% EMF at rated windspeed for battery connection
V_batt_nom_AC=(V_batt_nom)/(sqrt(3)*1.35);
EMF_nom=(V_batt_nom_AC+(V_batt_nom_AC*((trans_loss)/100)))/efficiency;
V_term_line=EMF_nom*efficiency;

% Flux per pole according to wind engineering paper
% (equations that do not take into account changes in ai kd)
% Bmg*wm*(10^-3)*la*(10^-3);

Flux_pole=Flux_pole_BOOK; 

% coils
coil_num=poles*0.75; % 3 coils to 4 magnets
nphase=3; % number of phases
q=coil_num/nphase; % Number of coils in phase group for 3 phase system
coil_FEMM=coil_num/q; % FEMM will use part of the stator according to phase coil number
magnet_num=coil_num/0.75; % number of rotor magnets, 3 coils to 4 magnets
magnet_FEMM=magnet_num/q; % FEMM will use only the part of the rotor that covers three coils

if stator_thick==1

else if stator_thick==2 && tw_Flag==1
    tw=13; % run a test run with tw=13
    tw_Flag=2; % change flag so that next time it doesn't enter
    else if stator_thick==2 && tw_Flag==2
        % theoretical calculation of stator thickness tw
        mrrec=Br/(4*pi*10^(-7)*Hc); % from Messinis p48
        tw_theor=(2/Bmax)*(((hm/mrrec)*(Br-Bmg))-Bmg*g); % from Messinis p49
        tw=ceil(tw_theor); % round to next integer number of mm
        tw_Flag=3; % change flag so that next time it doesn't enter
        end
    end
end

Nc_init=(sqrt(2)*EMF_cut_in)/(q*2*3.14*kw*Flux_pole*n_cut_in*poles/120); % Number of turns Nc per coil
Nc=round(Nc_init); % round to exact number of turns
if V_batt>=124
    tsr_nom=design_tsr; % due to MPPT
    n_nom=(60*Vw_nom*design_tsr)/(2*3.14*Rturb); % due to MPPT
    EMF_nom=(n_nom/n_cut_in)*EMF_cut_in; % nominal EMF for battery connection
else
    n_nom=(sqrt(2)*EMF_nom)/(Nc*q*2*3.14*kw*Flux_pole*poles/120); % rated rpm
    tsr_nom=(n_nom*2*3.14*Rturb)/(60*Vw_nom); % nominal tsr
end
fnom=poles*n_nom/120; % nominal frequency
Iac_nom=(Pnom)/(3*EMF_nom*efficiency); % rated AC current

tc=222.25*cq^2+129*cq+24.528; % from temperature tests Vasilis
pt=p20*(1+0.0039*(tc-20)); % specific resistance of copper at operating temp
resist_cop=pt; % electrical resistivity of copper Ohm.m at coil operating temperature

% Change fill factor due to multiple wires at hand when in calculated mode
if fill_factor==1

else if fill_factor==2 && kf_Flag==1
        if coil_type==1 && V_batt<=124
            kf=0.57; % run a test run with kf=0.57 for rectangular coils
            kf_Flag=2; % change flag so that next time it doesn't enter
        elseif V_batt>124 % high voltage stator other than 12,24,48,96 VDC systems
            kf=0.65; % run a test run with kf=0.65 for either coil type
            kf_Flag=2; % change flag so that next time it doesn't enter
        else
            kf=0.52; % run a test run with kf=0.52 for triangular coils
            kf_Flag=2; % change flag so that next time it doesn't enter
        end
else if fill_factor==2 && kf_Flag==2
        if coil_type==1 && V_batt<=124
            kf=0.57-(No_wires_at_hand-1)*0.02; % Reduce kf by 0.2 for each wire at hand
        elseif V_batt>124 % high voltage stator other than 12,24,48,96 VDC systems
            kf=0.65-(No_wires_at_hand-1)*0.02; % run a test run with kf=0.65 for either coil type
        else
            kf=0.52-(No_wires_at_hand-1)*0.02; % Reduce kf by 0.2 for each wire at hand
        end
    end
    end
end

wc_theor=((Iac_nom*Nc)/(sqrt(2*cq*(10^4)*kf*tw*(10^-3)/resist_cop)))*(10^3); %'wc' coil leg width in mm
sc_theor=Iac_nom/Jmax;

select_conductor % select conductor from commercialy availabel sizes

wc=(sc*Nc)/(tw*kf); % wc after choosing commercial conductor size

% rotor disks

% Back iron disk thickness
if rotor_thick==1

else if rotor_thick==2
        hr=round(hm*0.75); % Change disk thickness to be the same as 75% of magnet thickness in calculated mode
        if strcmp(mag_mater,'Ceramic 8')
            hr=round(hm*0.5); % Less for Ferrites
        end
    end
end

% For triangular coils calculate Ravg. For rectangular coils calculate Rin.
% It is assumed that the coil hole at Ravg and Rin is equal to the magnet
% width wm for triangular and rectangular coils respectively.

if coil_type==2 % for double layer concentrated winding (triangular coil)
    coil_side_dist=0.5; % Space between coils at Ravg
    wc=wc*1.01; % Add 1% to radius for triangular coils so that they fit
    Ravg=round_2dec((2*q*nphase*wc+q*nphase*wm+q*nphase*coil_side_dist)/(2*3.14)); % 'Ravg' radius in the middle of the effective length
    L_half=round_2dec(2*(pi)*Ravg/q); % Part of the perimeter at Ravg to be drawn in FEMM acccording to number of coils
    Rin=Ravg-la/2; % Rin is Ravg minus half the effective length la
    Rout_eff=Ravg+la/2; % The outer radius of the effective length of the generator
    % calculate segment due to magnet placed as a chord on the disk
    rts=roots([1 2*(la+Rin) -(wm/2)^2]); % Rout=segment+la+Rin
    for i=1:2
        if rts(i,1)>0
            segment=rts(i,1);
        end
    end
    Rout=Rin+la+segment; % 'Rout' outer radius of disks
    Dout=2*Rout; % outside diameter
end

if coil_type==3 % F series HP triangular coils
    coil_side_dist=0.5; % Space between coils 
    Rin=((wm+0.5)*poles)/(2*pi()); % so that magnets are 0.5 mm close to each other at Rin 
    Ravg=Rin+la/2;
    Rout_eff=Ravg+la/2;
    coil_side_dist=0.5; % Space between coils at Ravg 
    L_half=round_2dec(2*(pi)*Ravg/q); % Part of the perimeter at Ravg to be drawn in FEMM acccording to number of coils
    % calculate segment due to magnet placed as a chord on the disk
    rts=roots([1 2*(la+Rin) -(wm/2)^2]); % Rout=segment+la+Rin
    for i=1:2
        if rts(i,1)>0
            segment=rts(i,1);
        end
    end
    Rout=Rout_eff+segment; % 'Rout' outer radius of disks
    Dout=2*Rout; % outside diameter
end

if coil_type==1 % for single layer concentrated winding (rectangular coil)
    coil_side_dist=0.5; % Space between coils 
    Rin=(2*q*nphase*wc+q*nphase*wm)/(2*3.14);
    % calculate segment due to magnet placed as a chord on the disk
    rts=roots([1 2*(la+Rin) -(wm/2)^2]); % Rout=segment+la+Rin
    for i=1:2
        if rts(i,1)>0
            segment=rts(i,1);
        end
    end
    Rout=Rin+la+segment; % 'Rout' outer radius
    Dout=2*Rout; % outside diameter
    Rout_eff=Rout-segment; % The outer radius of the effective length of the generator
    Ravg=round_2dec(Rout_eff-la/2); % 'Ravg' radius in the middle of the effective length
    L_half=round_2dec(2*(pi)*Ravg/q); % Part of the perimeter at Ravg to be drawn in FEMM acccording to number of coils
end

% Back iron disk mass
BackIronVolume=2*hr*pi*(Rout^2)*10^(-3);% 2 rotor disks volume in cm3 / add -0.4*2*hr*pi*(Rin^2)*10^(-3); if whole in back iron disk. In this case 40% less material.
BackIronMass=BackIronVolume*pFe*10^(-3); % 2 rotor disks mass in kgr

if rotor == 0
    BackIronVolume=hr*pi*(Rout^2)*10^(-3);% 1 rotor disks volume in cm3 / add -0.4*2*hr*pi*(Rin^2)*10^(-3); if whole in back iron disk. In this case 40% less material.
    BackIronMass=BackIronVolume*pFe*10^(-3); % 1 rotor disks mass in kgr
end

% kd
kd=Rin/Rout_eff; % inner to outer radius ratio

%Design coils
if coil_type==2 % for double layer concentrated winding (triangular coil)
    coil_spacing=coil_side_dist; % make these variables the same
    coil_whole=wm;%(round_2dec((2*(pi)*Ravg-((2*wc+coil_side_dist)*coil_num))/coil_num)); % length of coil whole at Ravg is wm
    thitam=pi()*poles/coil_num; % calculation for lavg of coil
    thitare=kd/(1+kd)*thitam;
    lec=2*(Rout_eff+Rin)*(thitam-0.6*thitare)/poles;
    lavg=2*la+lec; % include kf in these calculation somehow??????
    Kn=(1+0.9*lavg/(2*pi*tw)+0.32*2*pi*wc/lavg+0.84*wc/tw)^(-1); % the Nagaoka constant for the calculation of phase inductance
    Ls=(q*((lavg/1000)^2)*(Nc^2)*(10^(-7))/(tw/1000))*Kn; % phase inductance
    coil_hole_Ravg=coil_whole;
    coil_hole_Rin=(2*pi*Rin-2*coil_num*wc-coil_side_dist*coil_num)/coil_num; % coil whole dimensions (arc)
    coilangle_Rin=360/(2*pi*Rin/coil_hole_Rin);
    coil_hole_Rin_constr=2*Rin*sin((coilangle_Rin*pi/180)/2);% coil whole dimensions (line)
    coil_hole_Rout=(2*pi*Rout_eff-2*coil_num*wc-coil_side_dist*coil_num)/coil_num; % coil whole dimensions (arc)
    coilangle_Rout=360/(2*pi*Rout_eff/coil_hole_Rout);
    coil_hole_Rout_constr=2*Rout_eff*sin((coilangle_Rout*pi/180)/2);% coil whole dimensions (line)
    if Radius_sim==2
        coil_whole_radius_sim=round_2dec(coil_hole_Rin);
    elseif Radius_sim==3
        coil_whole_radius_sim=round_2dec(coil_hole_Rout);
    elseif Radius_sim==1
        coil_whole_radius_sim=coil_whole;
    else
    end
end

if coil_type==3 % F series HP triangular coils
    coil_whole=(wm/2+winder_bolt_size/2); %round_2dec((2*(pi)*Ravg-((2*wc+coil_side_dist)*coil_num))/coil_num); % length of coil whole at Ravg is wm
    coil_spacing=round_2dec((2*(pi)*Ravg-((2*wc+coil_whole)*coil_num))/coil_num); % Space between coils at Ravg
    thitam=pi*poles/coil_num; % calculation for lavg of coil
    thitare=kd/(1+kd)*thitam;
    lec=2*(Rout_eff+Rin)*(thitam-0.6*thitare)/poles;
    lavg=2*la+lec; % include kf in these calculation somehow??????
    Kn=(1+0.9*lavg/(2*pi*tw)+0.32*2*pi*wc/lavg+0.84*wc/tw)^(-1); % the Nagaoka constant for the calculation of phase inductance
    Ls=(q*((lavg/1000)^2)*(Nc^2)*(10^(-7))/(tw/1000))*Kn; % phase inductance
    coil_hole_Ravg=coil_whole;
    coil_hole_Rin_constr=winder_bolt_size; %(2*pi*Rin-2*coil_num*wc-coil_side_dist*coil_num)/coil_num; % coil whole dimensions (arc)
    %coilangle_Rin=360/(2*pi*Rin/coil_hole_Rin);
    %coil_hole_Rin_constr=2*Rin*sin((coilangle_Rin*pi/180)/2);% coil whole dimensions (line)
    %coil_hole_Rout=(2*pi*(Rout_eff/1.06)-2*coil_num*wc-coil_side_dist*coil_num)/coil_num; % coil whole dimensions (arc)
    %coilangle_Rout=360/(2*pi*Rout_eff/coil_hole_Rout);
    coil_hole_Rout_constr=wm; %2*Rout_eff*sin((coilangle_Rout*pi/180)/2);% coil whole dimensions (line)
    if Radius_sim==2
        coil_whole_radius_sim=round_2dec(coil_hole_Rin_constr);
    elseif Radius_sim==3
        coil_whole_radius_sim=round_2dec(coil_hole_Rout_constr);
    elseif Radius_sim==1
        coil_whole_radius_sim=coil_whole;
    else
    end
end

if coil_type==1 % for single layer concentrated winding (rectangular coil)
    coil_whole=wm; % length of coil whole at Ravg is wm
    coil_hole_Ravg=coil_whole;
    coil_hole_Rin_constr=wm;
    coil_hole_Rout_constr=wm;
    coil_spacing=round_2dec((2*(pi)*Ravg-((2*wc+coil_whole)*coil_num))/coil_num); % Space between coils at Ravg
    lavg=2*(wm+la)+3.14*wc; % average length of a turn or wire in coil, from HP recipe book p56
    Kn=(1+0.9*lavg/(2*pi*tw)+0.32*2*pi*wc/lavg+0.84*wc/tw)^(-1); % NOT GOOD FOR THIS TYRP OF COIL the Nagaoka constant for the calculation of phase inductance
    Ls=(q*((lavg/1000)^2)*(Nc^2)*(10^(-7))/(tw/1000))*Kn; % NOT GOOD phase inductance
    if Radius_sim==2
        coil_whole_radius_sim=coil_whole;
    elseif Radius_sim==3
        coil_whole_radius_sim=coil_whole;
    elseif Radius_sim==1
        coil_whole_radius_sim=coil_whole;
    else
    end
end

% Radius to run simulation
if coil_type==1
    if Radius_sim==1
        L_half=round_2dec(2*(pi)*Ravg/q); % Part of the perimeter at Ravg to be drawn in FEMM acccording to number of coils
        dist_magnet=round_2dec((2*pi*Ravg-magnet_num*wm)/magnet_num); % Distance between magnets at Ravg
        coil_spacing=round_2dec((2*(pi)*Ravg-((2*wc+coil_whole)*coil_num))/coil_num); % Space between coils at Ravg
    elseif Radius_sim==2
        Rin=Rout-la+0.25; % recalculate inner radius
        L_half=round_2dec(2*(pi)*Rin/q); % Part of the perimeter at Rin to be drawn in FEMM acccording to number of coils
        dist_magnet=round_2dec((2*pi*Rin-magnet_num*wm)/magnet_num); % Distance between magnets at Rin
        coil_spacing=round_2dec((2*(pi)*Rin-((2*wc+coil_whole)*coil_num))/coil_num); % Space between coils at Rin
    elseif Radius_sim==3
        L_half=round_2dec(2*(pi)*Rout_eff/q); % Part of the perimeter at Rout_eff to be drawn in FEMM acccording to number of coils
        dist_magnet=round_2dec((2*pi*Rout_eff-magnet_num*wm)/magnet_num); % Distance between magnets at Rout_eff
        coil_spacing=round_2dec((2*(pi)*Rout_eff-((2*wc+coil_whole)*coil_num))/coil_num); % Space between coils at Rout_eff
    else
    end
end
  
if coil_type==2
    if Radius_sim==1
        L_half=round_2dec(2*(pi)*Ravg/q); % Part of the perimeter at Ravg to be drawn in FEMM acccording to number of coils
        dist_magnet=round_2dec((2*pi*Ravg-magnet_num*wm)/magnet_num); % Distance between magnets at Ravg
        coil_spacing=coil_side_dist; % make these variables the same
    elseif Radius_sim==2
        Rin=Rout-la+1; % recalculate inner radius
        L_half=round_2dec(2*(pi)*Rin/q); % Part of the perimeter at Rin to be drawn in FEMM acccording to number of coils
        dist_magnet=round_2dec((2*pi*Rin-magnet_num*wm)/magnet_num); % Distance between magnets at Rin
        coil_spacing=coil_side_dist; % make these variables the same
    elseif Radius_sim==3
        L_half=round_2dec(2*(pi)*Rout_eff/q); % Part of the perimeter at Rout_eff to be drawn in FEMM acccording to number of coils
        dist_magnet=round_2dec((2*pi*Rout_eff-magnet_num*wm)/magnet_num); % Distance between magnets at Rout_eff
        coil_spacing=coil_side_dist; % make these variables the same
    else
    end
end

if coil_type==3
    if Radius_sim==1
        L_half=round_2dec(2*(pi)*Ravg/q); % Part of the perimeter at Ravg to be drawn in FEMM acccording to number of coils
        dist_magnet=round_2dec((2*pi*Ravg-magnet_num*wm)/magnet_num); % Distance between magnets at Ravg
        coil_spacing=round_2dec((2*(pi)*Ravg-((2*wc+coil_whole_radius_sim)*coil_num))/coil_num); % Space between coils at Ravg
    elseif Radius_sim==2
        L_half=round_2dec(2*(pi)*Rin/q); % Part of the perimeter at Rin to be drawn in FEMM acccording to number of coils
        dist_magnet=round_2dec((2*pi*Rin-magnet_num*wm)/magnet_num); % Distance between magnets at Rin
        coil_spacing=round_2dec((2*(pi)*Rin-((2*wc+coil_whole_radius_sim)*coil_num))/coil_num); % Space between coils at Rin
    elseif Radius_sim==3
        L_half=round_2dec(2*(pi)*Rout_eff/q); % Part of the perimeter at Rout_eff to be drawn in FEMM acccording to number of coils
        dist_magnet=round_2dec((2*pi*Rout_eff-magnet_num*wm)/magnet_num); % Distance between magnets at Rout_eff
        coil_spacing=round_2dec((2*(pi)*Rout_eff-((2*wc+coil_whole_radius_sim)*coil_num))/coil_num); % Space between coils at Rout_eff
    else
    end
end

% ai 
dist_magnet_Rin=round_2dec((2*pi*Rin-magnet_num*wm)/magnet_num); % Distance between magnets at Rin
ai=wm/(wm+dist_magnet); % pole arc to pole pitch (magnet width) ratio

% calculation of coil resitance at operating temp
Rc=pt*Nc*lavg*10^(-3)/(sc*10^(-6)); % coil resistance
Rphase=Rc*q; % phase resistance

%Resistance calculation HP style!
lavgHP=lavg;
length__wire_HP=lavgHP*(Nc+2); % in mm + add 2 turns about 20cm each due to connection wire of coil at the two edges
ResHP_20=(length__wire_HP)/sc/56000; % in ohm at 20 Celsius
ResHP_70=ResHP_20*1.25; % at 70 Celsius
coil_weight_constr=sc*length__wire_HP*0.00894; % coil weight g
mcu_constr=(coil_weight_constr*(coil_num+1))/1000; % stator weight kgr + 1 extra coils

% Total magnet mass
VolumeMagnet=la*wm*hm*magnet_num*2; % Total magnet volume of 2 rotors in mm3
MagnetMass=mag_density*VolumeMagnet*10^(-3); % Magnet mass in g

% Hub mass
if Rturb<1.7
    No_bolts_hub=4;
    hub_radius=10; % bearing hub radius in cm
    hub_thickness=1.5; % bearing hub thickness in cm
elseif Rturb>=1.7 && Rturb<2.7
    No_bolts_hub=5;
    hub_radius=15; % bearing hub radius in cm
    hub_thickness=1.5; % bearing hub thickness in cm
elseif Rturb>=2.7 && Rturb<3.7
    No_bolts_hub=6;
    hub_radius=17; % bearing hub radius in cm
    hub_thickness=2; % bearing hub thickness in cm
elseif Rturb>=3.7 && Rturb<=5
    No_bolts_hub=8;
    hub_radius=20; % bearing hub radius in cm
    hub_thickness=3; % bearing hub thickness in cm
else
end
BearingHubMass=pi*hub_radius^2*hub_thickness*pFe; % Rotational mass of bearing hub in g (length and radius in cm)

% Blade rotor mass in kgr
blade_rotor_volume=3*(Rturb*(Rturb/29.5)*(Rturb/9.5)/2.8); % in m3 - 0.00239m3 from 20005 blade scan - from recipe book lade lenght to windth 9.5 and blade length to thickness 29.5 and also 2005 blade volume
softwood_density=500; % for pine about 500kgr/m3
blade_rotor_mass=blade_rotor_volume*softwood_density; % in kgr

% calculation of losses and calculation of real efficiency at rated rpm
Peddy=(pi*la*10^(-3)*((dc_theor*10^(-3))^4)*(Bp^2)*((2*pi*n_nom*poles/120)^2)*coil_num*Nc)/(32*pt); % eddy current losses
% need to add windage losses to rotational losses!!!
Prot=0.06*2*(BackIronMass+blade_rotor_mass+MagnetMass*10^(-3)+BearingHubMass*10^(-3))*n_nom/60; % rotational losses (div by rev/s)
Pcopper=3*Iac_nom^2*q*Rc; % ohmic losses
Plosses=Pcopper+Peddy+Prot; % total losses
Phl=3*Iac_nom*EMF_nom*efficiency; % electrical power
efficiency_real=(Phl/(Phl+Plosses)); % real efficiency

% Calulate real cq
stator_surface_area=2*q*nphase*(wc*lavg)*(10^-2); % coil surface area in cm2
cq_real=Pcopper/stator_surface_area;
tc_real=222.25*cq_real^2+129*cq_real+24.528; % from temperature tests Vasilis
cq=cq_real;
Jmax_real=Iac_nom/sc;

% Calculate total cost of generator

% Magnet cost as of January 2018
Vmag=wm*la*hm;
shippingcost=Vmag*2*poles*20/(24*13800);
 if strcmp(mag_mater,'Ceramic 8')
    Magcost=(0.4105+10^(-5)*Vmag)*2*poles+shippingcost;
 elseif strcmp(mag_mater,'NdFeB N35')
    Magcost=(0.5795*Vmag*10^(-3)+0.3967)*2*poles+shippingcost;
 elseif strcmp(mag_mater,'NdFeB N40')
    Magcost=(0.5995*Vmag*10^(-3)+0.5)*2*poles+shippingcost;
 elseif strcmp(mag_mater,'NdFeB N42')
    Magcost=(0.6185*Vmag*10^(-3)+0.536)*2*poles+shippingcost;
 elseif strcmp(mag_mater,'NdFeB N45')
    Magcost=(0.6751*Vmag*10^(-3)+0.27038)*2*poles+shippingcost;
 elseif strcmp(mag_mater,'NdFeB N52')
    Magcost=(0.7971*Vmag*10^(-3)+0.0552)*2*poles+shippingcost;
 end

% Copper cost
mcu=3*Nc*q*lavg*sc*pcu*(10^(-6)); % in kg
coppercost=1.23*copperprice*mcu;

% Iron cost
ironcost=1.23*ironprice*BackIronMass;

% Resin cost
coil_volume=wc*lavg*tw; % coil volume in mm3
ResinVolume=(2*(pi*((Rout+5)^2-(Rin-20)^2)*tw*10^(-3)))-(2*poles*Vmag*10^(-3))+((pi*((Rout+wc+10)^2-(Rin-wc)^2)*tw*10^(-3)))-(coil_num*coil_volume*10^(-3)); % in cm3
resinmass=presin*ResinVolume*10^(-3);% in kg
resincost=1.23*resinmass*resinprice;

% Wood cost (for moulds)
woodS=(2*(Rout*10^(-3)+0.15))^2; % Surface area of square moulds
woodcost=3*woodS*pplywood+6*woodS*pplywood; % Euro

%Total cost of generator
Totalcost=Magcost+coppercost+ironcost+resincost+woodcost;

%Total mass of generator
TotalMass=mcu+resinmass+BackIronMass+MagnetMass/1000;

%Total mass of 1 rotor
TotalMassRotor=(BackIronMass+MagnetMass/1000)/2;

if rotor == 0
    TotalMassRotor=(BackIronMass+MagnetMass/1000);
end

%Total volume
if rotor == 2
    TotalVolume= pi*Rout^2*(hr+hm+g+tw+g+hm+hr)/10^6;
end

if rotor == 1
    TotalVolume= pi*Rout^2*(hr+g+tw+g+hm+hr)/10^6;
end

if rotor == 0
    TotalVolume= pi*Rout^2*(tw+g+hm+hr)/10^6;
end

% Construction limitations
if ((2*pi*Rin-magnet_num*wm)/magnet_num)<0
    no_FEMM=1;
    break % don't make the desing
end

if coil_spacing<coil_side_dist
    no_FEMM=1;
    break % don't make the desing
end

if sc_multi_wires==2.84 % if more than 8 wires at hand and more than 1.9mm copper diameter
    no_FEMM=1;
    break % don't make the desing
end

Bp_ai_kd_WindEng_18_12_16 % draw the rotor to calculate Bp and Bmg

Flux_pole_BOOK=ai*Bmg*(3.14/(8*poles/2))*((Rout*2*0.001)^2)*(1-kd^2); % from AFPM book page 40

if (kw_counter==1 && coil_type==3) || (kw_counter==1 && rotor==1) || (kw_counter==1 && rotor==0)
    kw=Vdc_OC*100/V_batt/100; % find out how much cut-in voltage is off and set kw
end
kw_counter=kw_counter+1; % do this once

Bp_real=Bpmax;
Bmg_real=Bmg;
Bmax_real=Bmax;

if break_counter==20 % check for 20 iterations then break
   break
end
break_counter=break_counter+1;

end

% Construction limitations
flag_solver=0; % Decide when to run solver and when not to
if ((2*pi*Rin-magnet_num*wm)/magnet_num)<0
    display('Construction limitation: It is not possible to build this magnet rotor because the magnets are overlapping at the inner radius of the generator. Try reducing the number of poles.')
    flag_solver=1;
end

if coil_spacing<coil_side_dist
    display('Construction limitation: It is not possible to build this stator because the coils are overlapping at the average radius of the generator. Try increasing the number of poles.')
    flag_solver=1;
end

if (coil_hole_Rin_constr-5)<=0
    display('Construction limitation: It is not possible to build this coil because its whole at the inner radius is less than 5mm. Try reducing the number of poles.')
    flag_solver=1;
end

if (tw/wc)<=0.25
    display('Construction limitation: It is not possible to build this stator because the leg width of the coils is more than three times the thickness of the stator. Try increasing the number of poles or the value of the current density.')
    flag_solver=1;
end

if (tw/wc)>2.5
    display('Construction limitation: It is not possible to build this stator because the thickness of the coils is more than twice the leg width of the coils. Try reducing the number of poles.')
    flag_solver=1;
end

if efficiency_real<0.65
    display('Warning: The efficiency of the generator is below 65% and it is advisable not to reduce it further. Consider increasing the number of poles or reducing the current density.')
end

if efficiency_real<0.55
    flag_solver=1;
end

if cq_real>0.4
    display('Warning: The heat coefficient is more than 0.4 and this will create temperatures in the stator of above 115 degrees Celsius. Consider increasing the number of poles or reducing the current density.')
end

if cq_real>0.55
    flag_solver=1;
end

if Jmax_real>6.5
    display('Warning: The current density is more than 6.5 and this will create high temperatures in the stator. Consider increasing the number of poles.')
end

if Jmax_real>8
    flag_solver=1;
end

if sc_multi_wires==2.84
   display('Construction limitation: It is not possible to build this stator because the coils require more than 8 wires at hand of 1.9mm copper wire diameter. Consider increasing the number of poles or the system voltage.')
   flag_solver=1;
end

if break_counter==20
   display('Construction limitation: It was not possible to design this generator after trying 20 times. Look carefully at your design.')
   flag_solver=1;
end

coil_whole_temp=coil_whole; % in order to include Radius Sim for all coils
coil_whole=coil_whole_radius_sim;

% Calculate back iron disk thickness for saturation at 1.5T
if no_FEMM==1 % if it is possible to draw the generator
    else
    CALC_hr_saturation;
end

% Calculate hr for no saturation when rotor_thick==2
if rotor_thick==2 && no_FEMM==0
   if SaturMAX>1.5
       while SaturMAX>1.5
           hr=hr+1; % increase hr by 1mm
           CALC_hr_saturation;
       end
   else
       while SaturMAX<=1.5
           hr=hr-1; % decrease hr by 1mm
           CALC_hr_saturation;
       end
       hr=hr+1;
       CALC_hr_saturation;
   end
else
end

% rotational speed
speed=n_nom;

% maximum current to simulate and current step, set to zero for no load operation
Imax=Iac_nom;
I_step=Iac_nom/current_step; % User defined with values 1,2,3,4 and 5

if flag_solver==0 && run_solver==true
    % open solver to run FEMM simulation
    display 'Running solver';
    solver;
    % find cut-in RPM
    RPMcutinCALC;
    % run performance
    display 'Running performance';
    performance_15_12_17_MagnAFPM;
else
end

coil_whole=coil_whole_temp; % in order to include Radius Sim for all coils

% get a screan shot of FEMM output and flux density
if no_FEMM==1 % if it is possibel to drwa the generator
    else
    mo_showdensityplot(1,0,0,2,'mag');
    path=[location_path,'FEMM_screenshot.bmp'];
    mo_savebitmap(path);
end
