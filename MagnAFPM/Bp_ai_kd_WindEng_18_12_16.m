% solver starts here

depth=la; % 'la' effective length is the depth of FEMM 2D - in this case 30mm
msize=1; % Local element size no greater than msize in FEMM
automesh_temp=automesh; % Get user value of mesh (0) Consider mesh size (1) automesh
automesh=0; % Always use fine mesh for MagnAFPM

if rotor == 2  || rotor == 0 % set hr=hr+hm for easy construction of second rotor
    hr=hr+hm;
end

openfemm; % open FEMM
newdocument(0); % new document
mi_probdef(0,'millimeters','planar',1E-8,depth,30,0); % define problem
msize=1; % Local element size no greater than msize

drawcoils_1800 % in order to take photo later

if rotor == 0 % extra space in the case of rotor (0) in order to allow for magnetic field to expand
    mem=tw;
    tw=tw+extra;
end

drawrotordisks_1800 % draws rotor disks in FEMM
drawmagnets_1800 % draws magnets in FEMM

% adds materials to be used in the problem
% Add materials to library
addmaterials_1800 % adds materials to be used in the problem
addcircuits_1800 % adds three phases of the circuit
addbounds_1800 % adds boundary conditions of arrangement

setmagnets_1800 % set magnets properties

if rotor == 0 % set tw back to original value
    tw=mem;
end

setcoils_1800 % in order to take photo later

if rotor == 0 % extra space in the case of rotor (0) in order to allow for magnetic field to expand
    tw=tw+extra;
end

drawbounds_1800 % draw and set boundary conditions
set_air_iron_1800 % set properties for air and rotor disks

if rotor == 0 % set tw back to original value
    tw=mem;
end

path=[location_path,'temp.fem'];
mi_saveas(path); % temporary FEMM file

mi_modifycircprop('A',1,0); % set currents in FEMM

mi_createmesh % create mesh in FEMM
mi_analyze(1) % analysis
mi_loadsolution % loads solution for post processor

BmgCALC % calculate Bmg for inital position
j=1; % for BpCALC
BpCALC % calculate Bp for inital position
BmaxCALC % calculate Bmax and Bavg in aigap

vectorA=mo_getcircuitproperties('A'); % get circuit prop
fluxlinkageA=vectorA(3); % return flux linkage
Va=q*fluxlinkageA*poles/2*2*pi*n_cut_in/60; % rms value of induced voltage in A
Vdc_OC=sqrt(3)*1.35*Va/sqrt(2); % turn peak voltage into RMS and then to DC

if rotor == 2  || rotor == 0 % set hr back to its original value
    hr=hr-hm;
end

automesh=automesh_temp; % Return user value of mesh (0) Consider mesh size (1) automesh
